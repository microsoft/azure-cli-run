# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.1.0

- minor: Upgrade docker image to mcr.microsoft.com/azure-cli:2.3.1.

## 1.0.3

- patch: Update support link and license to Microsoft

## 1.0.2

- patch: REMOVE redundant echo from the pipe script

## 1.0.1

- patch: Update license to MIT

## 1.0.0

- major: Initial release.

