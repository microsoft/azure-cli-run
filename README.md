# Bitbucket Pipelines Pipe: Azure CLI Deploy

A pipe to authenticate to Azure and run ad-hoc commands using the [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/)

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: microsoft/azure-cli-run:1.1.0
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      CLI_COMMAND: 'az $CLI_COMMAND'
      # DEBUG: DEBUG # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AZURE_APP_ID (*)        | The app ID, URL or name associated with the service principal required for login. |
| AZURE_PASSWORD (*)      | Credentials like the service principal password, or path to certificate required for login. |
| AZURE_TENANT_ID  (*)    | The AAD tenant required for login with the service principal. |
| CLI_COMMAND           | A string representing the Azure cli command |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

You will need to configure a service principal with access to the resource you'd like to interact with.

### Documentation

* [Create an Azure service principal with Azure CLI](https://docs.microsoft.com/en-us/cli/azure/create-an-azure-service-principal-azure-cli)

### Instructions

Create a service principal in your Azure subscription:

```sh
az ad sp create-for-rbac --name MyServicePrincipal
```

## Examples

Basic example:

```yaml
script:
  - pipe: microsoft/azure-cli-run:1.1.0
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      CLI_COMMAND: 'az account show'
```

Advanced example:

```yaml
script:
  - pipe: microsoft/azure-cli-run:1.1.0
    variables:
      AZURE_APP_ID: $AZURE_APP_ID
      AZURE_PASSWORD: $AZURE_PASSWORD
      AZURE_TENANT_ID: $AZURE_TENANT_ID
      CLI_COMMAND: 'az aks create --resource-group myResourceGroup --name myAKSCluster --node-count 1 --enable-addons monitoring --generate-ssh-keys'
      DEBUG: 'true'
```

## Support

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance. To discuss this sample with other users, please visit the Azure DevOps Services section of the Microsoft Developer Community: https://developercommunity.visualstudio.com/spaces/21/index.html.
